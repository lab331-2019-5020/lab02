import { StudentService } from "./student-service";
import { Observable, of } from "rxjs";
import { Student } from "../entity/student";

export class MyStudentData extends StudentService{
    constructor(){
        super();
    }
    getStudents(): Observable<Student[]>{
        return of(this.students);
    };
     
     students: Student[] = [{
         'id': 1,
         'studentId': '602115020',
         'name': 'Poomrapee',
         'surname': 'Kanthapong',
         'gpa': 2.53
       }];
}
