import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl-service';
import { MyStudentData } from './service/my-student-data';



@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: StudentService, useClass: MyStudentData}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
